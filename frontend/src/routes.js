import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Welcome from "././components/Welcome/Welcome";
import Home from "././components/Home/Home";
import HomeEdit from "././components/Home/Edit";
import Login from "././components/Login/Login";
import NotFound from "././components/NotFound/NotFound";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/home/new" component={HomeEdit} />
      <Route path="/home/edit/:id" component={HomeEdit} />
      <Route path="/home" component={Home} />
      <Route path="/login" component={Login} />
      <Route path="*" component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
