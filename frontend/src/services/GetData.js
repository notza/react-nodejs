export function GetData(type, userData) {
  let BaseURL = "http://localhost:5000/api/";
  //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';

  return new Promise((resolve, reject) => {
    fetch(BaseURL + type, {
      method: "GET",
      body: JSON.stringify(userData)
    })
      .then(response => response.json())
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
