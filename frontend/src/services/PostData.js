export function PostData(type, userData) {
  let BaseURL = "http://localhost:5000/api/";
  //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';

  return new Promise((resolve, reject) => {
    fetch(BaseURL + type, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(userData)
    })
      .then(response => response.json())
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function PutData(type, userData) {
  let BaseURL = "http://localhost:5000/api/";
  //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';

  return new Promise((resolve, reject) => {
    fetch(BaseURL + type, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(userData)
    })
      .then(response => response.json())
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function GetData(type, userData) {
  let BaseURL = "http://localhost:5000/api/";
  //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';

  return new Promise((resolve, reject) => {
    fetch(BaseURL + type)
      .then(response => response.json())
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function DeleteData(type, userData) {
  let BaseURL = "http://localhost:5000/api/";
  //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';
  console.log(userData);
  return new Promise((resolve, reject) => {
    fetch(BaseURL + type + "/" + userData.id, {
      method: "delete"
    })
      .then(response => response.json())
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
