import React, { Component } from "react";
import { connect } from "react-redux";

class LoginForm extends Component {
  handerSubmit = e => {
    e.preventDefault();
    const name = this.getName.value;
    const message = this.getMessage.value;
    const data = {
      id: new Date(),
      name,
      message
    };
    this.getName.value = "";
    this.getMessage.value = "";
    this.props.dispatch({
      type: "ADD_COMMENT",
      data
    });
  };
  render() {
    return (
      <div>
        <h1>Login From</h1>
        <form onSubmit={this.handerSubmit}>
          <input
            required
            type="text"
            placeholder="Username"
            ref={input => (this.getName = input)}
          />
          <br />
          <br />
          <input
            required
            rows="5"
            cols="20"
            placeholder="Paswword"
            ref={input => (this.getMessage = input)}
          />
          <br />
          <br />
          <button>Login</button>
        </form>
      </div>
    );
  }
}

export default connect()(LoginForm);
