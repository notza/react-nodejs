import React, { Component } from "react";
import { connect } from "react-redux";

class ShowComment extends Component {
  render() {
    return (
      <div>
        <h1>All Comment</h1>
        {console.log(this.props.comments)}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    comments: state
  };
};
export default connect(mapStateToProps)(ShowComment);
