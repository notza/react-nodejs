const commentReducer = (state = [], action) => {
  //รูปแบบ Action
  switch (action.type) {
    case "ADD_COMMENT":
      return state.concat([action.data]);

    default:
      return state;
  }
};
export default commentReducer;
