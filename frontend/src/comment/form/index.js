import React, { Component } from "react";
import { connect } from "react-redux";

class CommentForm extends Component {
  handerSubmit = e => {
    e.preventDefault();
    const name = this.getName.value;
    const message = this.getMessage.value;
    const data = {
      id: new Date(),
      name,
      message
    };
    this.getName.value = "";
    this.getMessage.value = "";
    this.props.dispatch({
      type: "ADD_COMMENT",
      data
    });
  };
  render() {
    return (
      <div>
        <h1>Add Comment</h1>
        <form onSubmit={this.handerSubmit}>
          <input
            required
            type="text"
            placeholder="Name"
            ref={input => (this.getName = input)}
          />
          <br />
          <br />
          <textarea
            required
            rows="5"
            cols="20"
            placeholder="Text Comemnt"
            ref={input => (this.getMessage = input)}
          />
          <br />
          <button>Comemnt</button>
        </form>
      </div>
    );
  }
}

export default connect()(CommentForm);
