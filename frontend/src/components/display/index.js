import React, { Component } from "react";
import CounterShow from "./../counter";
import ClickButton from "./../button";

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = { number: 1, alertMessage: "" }; //default
    this.clickPlus = this.clickPlus.bind(this);
    this.clicktest = this.clicktest.bind(this);
  }

  clicktest(value) {
    if (value === 0) {
      this.setState({ alertMessage: "No zero" });
      return 0;
    } else {
      return value - 1;
    }
    return 665;
  }
  clickPlus() {
    this.setState({ alertMessage: "" });
    this.setState({ number: this.state.number + 1 });
  }

  plusplus = () => {
    this.setState({ number: this.clicktest(this.state.number) });
  };

  render() {
    return (
      <div className="App">
        <ClickButton clickPlus={this.clickPlus} text={"+"} />
        <CounterShow number={this.state.number} />
        <ClickButton clickPlus={this.plusplus} text={"-"} />
        {/* <div>{this.state.alertMessage}</div> */}
        {this.state.number === 0 && <div>No 0 </div>}
      </div>
    );
  }
}

export default Counter;
