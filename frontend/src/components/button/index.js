import React, { Component } from "react";
import Buttonheader from "./header";

class ClickButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Buttonheader />
        <button onClick={this.props.clickPlus}>{this.props.text}</button>
      </div>
    );
  }
}

export default ClickButton;
