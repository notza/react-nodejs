import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import "./Home.css";
import { PostData, GetData, DeleteData } from "../../services/PostData";
import { Link } from "react-router-dom";
// import { GetData } from "../../services/GetData";

import UserFeed from "../UserFeed/UserFeed";
import ReactConfirmAlert, { confirmAlert } from "react-confirm-alert";
import "../../styles/react-confirm-alert.css";

class HomeIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      redirectToReferrer: false,
      name: ""
    };

    this.getUserFeed = this.getUserFeed.bind(this);
    this.feedUpdate = this.feedUpdate.bind(this);
    this.deleteFeed = this.deleteFeed.bind(this);
    this.deleteFeedAction = this.deleteFeedAction.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {
    if (sessionStorage.getItem("userData")) {
      this.getUserFeed();
    } else {
      this.setState({ redirectToReferrer: true });
    }
  }

  feedUpdate(e) {
    e.preventDefault();
    let data = JSON.parse(sessionStorage.getItem("userData"));
    let postData = {
      user_id: data.userData.user_id,
      token: data.userData.token,
      name: this.state.userFeed
    };
    if (this.state.userFeed) {
      PostData("post", postData).then(result => {
        let responseJson = result;
        console.log(result);
        let K = [responseJson.feedData].concat(this.state.data);
        console.log(K);
        this.setState({ data: K, userFeed: "" });
      });
    }
  }

  deleteFeedAction(e) {
    let updateIndex = e.target.getAttribute("value");
    let feed_id = e.target.getAttribute("data");
    let data = JSON.parse(sessionStorage.getItem("userData"));

    let postData = {
      user_id: data.userData.user_id,
      token: data.userData.token,
      id: feed_id
    };
    if (postData) {
      DeleteData("post", postData).then(result => {
        this.state.data.splice(updateIndex, 1);
        this.setState({ data: this.state.data });
      });
    }
  }

  deleteFeed(e) {
    // console.log(e.target.getAttribute("value"));
    this.deleteFeedAction(e);
    // confirmAlert({
    //   title: "",
    //   message: "Are you sure?",
    //   childrenElement: () => "",
    //   confirmLabel: "Delete",
    //   cancelLabel: "Cancel",
    //   onConfirm: () => this.deleteFeedAction(e),
    //   onCancel: () => ""
    // });
  }

  getUserFeed() {
    let data = JSON.parse(sessionStorage.getItem("userData"));
    this.setState({ name: data.userData.firstname });
    let postData = {
      user_id: data.userData.id,
      token: data.userData.token
    };

    if (data) {
      GetData("post", postData).then(result => {
        let responseJson = result;
        this.setState({ data: responseJson.feedData });
      });
    }
  }
  logout() {
    sessionStorage.setItem("userData", "");
    sessionStorage.clear();
    this.setState({ redirectToReferrer: true });
  }

  render() {
    if (this.state.redirectToReferrer) {
      return <Redirect to={"/login"} />;
    }

    return (
      <div className="row" id="Body">
        <div className="medium-12 columns">
          <a href="#" onClick={this.logout} className="logout">
            Logout
          </a>
          <Link to={"/home/new"}>Add </Link>
        </div>
        <UserFeed feedData={this.state.data} deleteFeed={this.deleteFeed} />
      </div>
    );
  }
}

export default HomeIndex;
