import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import "./Home.css";
import {
  PostData,
  GetData,
  DeleteData,
  PutData
} from "../../services/PostData";
// import { GetData } from "../../services/GetData";

import UserFeed from "../UserFeed/UserFeed";
import ReactConfirmAlert, { confirmAlert } from "react-confirm-alert";
import "../../styles/react-confirm-alert.css";

class HomeEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      userFeed: "",
      postId: this.props.match.params.id,
      redirectToReferrer: false,
      name: ""
    };

    this.feedUpdate = this.feedUpdate.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    if (sessionStorage.getItem("userData")) {
      if (this.state.postId) {
        let data = JSON.parse(sessionStorage.getItem("userData"));
        let postData = {
          user_id: data.userData.user_id,
          token: data.userData.token,
          name: this.state.userFeed
        };
        let loadData = "post/" + this.state.postId;

        GetData(loadData, postData).then(result => {
          this.setState({
            userFeed: result.name
          });
        });
      }
    } else {
      this.setState({ redirectToReferrer: true });
    }
  }

  feedUpdate(e) {
    e.preventDefault();
    let data = JSON.parse(sessionStorage.getItem("userData"));
    if (this.state.postId) {
      let postData = {
        user_id: data.userData.user_id,
        token: data.userData.token,
        name: this.state.userFeed,
        id: this.state.postId
      };
      let urlPut = "post/" + this.state.postId;
      if (this.state.userFeed) {
        PutData(urlPut, postData).then(result => {
          if (result.status == 1) {
            this.props.history.push("/home");
          }
        });
      }
    } else {
      let postData = {
        user_id: data.userData.user_id,
        token: data.userData.token,
        name: this.state.userFeed
      };
      if (this.state.userFeed) {
        PostData("post", postData).then(result => {
          this.props.history.push("/home");
        });
      }
    }
  }

  onChange(e) {
    this.setState({ userFeed: e.target.value });
  }
  logout() {
    sessionStorage.setItem("userData", "");
    sessionStorage.clear();
    this.setState({ redirectToReferrer: true });
  }

  render() {
    if (this.state.redirectToReferrer) {
      return <Redirect to={"/login"} />;
    }

    return (
      <div className="row" id="Body">
        <div className="medium-12 columns">
          <a href="#" onClick={this.logout} className="logout">
            Logout
          </a>
          <form onSubmit={this.feedUpdate} method="post">
            <input
              name="userFeed"
              onChange={this.onChange}
              value={this.state.userFeed}
              type="text"
              placeholder="Text Post"
            />
            <input
              type="submit"
              value="Post"
              className="button"
              onClick={this.feedUpdate}
            />
          </form>
        </div>
      </div>
    );
  }
}

export default HomeEdit;
