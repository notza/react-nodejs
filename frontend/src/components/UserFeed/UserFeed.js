import React, { Component } from "react";
import "./UserFeed.css";
import { Link } from "react-router-dom";
class UserFeed extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let userFeed = this.props.feedData.map(function(feedData, index) {
      return (
        <div className="medium-12 columns" key={index}>
          <div className="people-you-might-know">
            <div className="row add-people-section">
              <div className="small-12 medium-10 columns about-people">
                <div className="about-people-author">
                  <p className="author-name">
                    <b>{feedData.name}</b>

                    <br />
                  </p>
                </div>
              </div>
              <div className="small-12 medium-2 columns add-friend">
                <div className="add-friend-action">
                  <Link
                    to={"/home/edit/" + feedData.id}
                    className="button secondary small"
                  >
                    Edit
                  </Link>
                  <button
                    className="button secondary small"
                    onClick={this.props.deleteFeed}
                    data={feedData.id}
                    value={index}
                  >
                    <i className="fa fa-user-times" aria-hidden="true" />
                    Delete
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }, this);

    return <div>{userFeed}</div>;
  }
}

export default UserFeed;
