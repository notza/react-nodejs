import React, { Component } from "react";

class CounterShow extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <span>{this.props.number}</span>;
  }
}

export default CounterShow;
