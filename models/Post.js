var db = require("../dbconnection");

var Post = {
  getAllPosts: function(callback) {
    return db.query("Select * from post", callback);
  },
  getPostById: function(id, callback) {
    return db.query("select * from post where id=?", [id], callback);
  },
  addPost: function(Post, callback) {
    return db.query(
      "Insert into post values(?,?,?)",
      ["", Post.name, ""],
      callback
    );
  },
  deletePost: function(id, callback) {
    return db.query("delete from post where id=?", [id], callback);
  },
  updatePost: function(id, Post, callback) {
    return db.query(
      "update post set name=? where id=?",
      [Post.name, id],
      callback
    );
  }
};
module.exports = Post;
