var db=require('../dbconnection');

var User={

	getAllUsers:function(callback){

		return db.query("Select * from user",callback);

	},
	getUserById:function(id,callback){

	    return db.query("select * from user where id=?",[id],callback);
	},
	addUser:function(User,callback){
	  	console.log("inside service");
	   	console.log(User);
		return db.query("Insert into user values(?,?,?,?,?,?)",['',User.username, User.password, User.email, User.firstname, User.lastname],callback);
		//return db.query("insert into User(id,username,email) values(?,?,?)",[User1.id,User1.username,User1.email],callback);
	},
	deleteUser:function(id,callback){
	    return db.query("delete from user where id=?",[id],callback);
	},
	updateUser:function(id,User,callback){
	    return  db.query("update user set username=?, email=?, firstname=?, lastname=? where id=?",[User.username,User.email,id,User.firstname,User.lastname],callback);
	},
};

module.exports=User;