var express = require('express');
var router = express.Router();
var Post=require('../models/Post');

router.get('/:id?',function(req,res,next){

    if(req.params.id){

        Post.getPostById(req.params.id,function(err,rows){

            if(err)
            {
                res.json(err);
            }
            else{                
                res.json(rows[0]);
            }
        });
    }
    else{

     Post.getAllPosts(function(err,rows){

            if(err)
            {
                res.json(err);
            }
            else
            {

                let getPostResult = {
                    message: '',
                    status: '0',
                    feedData: rows,
                };
                if(rows[0]){
                    getPostResult.message = "Post All";
                    getPostResult.status = 1;
                }
                res.json(getPostResult);
            }
     
        });
    }
});

router.post('/',function(req,res,next){
    
    Post.addPost(req.body,function(err,rows){

        
        if(err)
        {
            res.json(err);
        }
        else{


            let addPostResult = {
                message: '',
                status: '0',
                feedData: req.body,
            };

            addPostResult.message = "Add Post Success";
            addPostResult.status = 1;
            
            res.json(addPostResult);

            //console.log(req.body);

            //res.json(req.body);//or return count for 1 & 0
        }
    });
});


router.delete('/:id',function(req,res,next){

    Post.deletePost(req.params.id,function(err,count){

        if(err)
        {
            res.json(err);
        }
        else
        {
            res.json(count);
        }

    });
});
router.put('/:id',function(req,res,next){

    Post.updatePost(req.params.id,req.body,function(err,rows){

        if(err)
        {
            res.json(err);
        }
        else
        {
            let updateResult = {
                message: 'Success',
                status: '1',
                rows: rows[0],
            };
            res.json(updateResult);
        }
    });
});
module.exports=router;